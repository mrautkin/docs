Установка под ubuntu
====================

https://docs.docker.com/engine/installation/linux/ubuntulinux/

## Установка Docker на Ubuntu 16.04

```
sudo apt-get update
```

Ключ репозитория докер:
```
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
```

Репозиторий докера:
```
echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list
```

Установка:
```
sudo apt-get update
apt-cache policy docker-engine
sudo apt-get install -y docker-engine
sudo systemctl status docker
```

Для работы без sudo добавляем пользователя в группу:
```
sudo usermod -aG docker $(whoami)
sudo usermod -aG docker your_current_username
```

Теперь нужно перелогиниться.

Установка docker-compose:
```
sudo apt-get -y install python-pip
sudo pip install docker-compose
```